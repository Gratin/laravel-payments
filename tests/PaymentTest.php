<?php
namespace Gratin\Payment\Tests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PaymentTest extends TestCase
{
    private $client;
    private $customerCode;

    public function setUp()
    {
        parent::setUp();
        $this->client = new \Gratin\Payment\Adapters\BamboraAdapter();
    }

    /**
     * Tests configuration setting
     *
     */
    public function testConfigration()
    {
    }
    /**
     */
    public function testCreateCustomer()
    {
        $cardData = [
            "number"        =>  "4030000010001234",
            "expiry_month"  =>  "02",
            "expiry_year"   =>  "20",
            "cvd"           =>  "123"
        ];

        $card = $this->client->createCard($cardData);
        $name     = "Test McTest";
        try {
            $profile  = $this->client->createCustomer([
                'token' =>  [
                    'code'  =>  $card['token'],
                    'name'  =>  $name,
                ],
                'billing'   =>  [
                    'name'              =>  $name,
                    'address_line1'     =>  '3451 Saint-Laurent',
                    'address_line2'     =>  'Bureau 4',
                    'postal_code'       =>  'H2X2T6',
                    'city'              =>  'Montreal',
                    'country'           =>  'CA',
                    'province'          =>  'Qc',
                    'email_address'     =>  'luc.olivier@quatrecentquatre.com',
                    'phone_number'      =>  '514-555-5555'
                ]
            ]);
        } catch (\Exception $e) {
            throw $e;
        }

        $this->customerCode = $profile['customer_code'];

        self::assertFalse(empty($profile));
    }

    /**
     */
    public function testCreateCreditCardSuccess()
    {
            $cardData = [
            "number"        =>  "4111111111111111",
            "expiry_month"  =>  "02",
            "expiry_year"   =>  "20",
            "cvd"           =>  "123"
        ];

        $card = $this->client->createCard($cardData);
        self::assertTrue($this->assertStructure(['token', 'code', 'version', 'message'], $card), self::isTrue());
    }

    /**
     */
    public function testCreateCreditCardFailure()
    {
        $cardData = [
            "number"        =>  "4111111111111111",
            "expiry_month"  =>  "02",
            "expiry_year"   =>  "15",
            "cvd"           =>  "123"
        ];

        $response = $this->client->createCard($cardData);
        $name     = "Test McTest";
        try {
            $profile  = $this->client->createCustomer(['token'=>['code'=>$response['token'], 'name'=>$name]]);
        } catch (\Exception $e) {
            $profile = false;
        }

        self::assertFalse($profile);
    }

    /**
     */
    public function testValidBillingInfo()
    {
    }

    /**
     */
    public function testInvalidBillingInfo()
    {
    }

    public function testGetCardsForCustomer()
    {
        $this->customerCode = "2443a0185fFD4CD4AcBBb645fc9f4005";

        $data   =   [
            'customer_code'     =>  $this->customerCode
        ];

        $cards = $this->client->getCards(['customer_code'=>$this->customerCode], $this->customerCode);

        self::assertTrue(!empty($cards['card']));
    }

    /**
     */
    public function testCreateChargeForExistingCustomer()
    {
        $this->customerCode = "2443a0185fFD4CD4AcBBb645fc9f4005";
        $data   =   [
            'customer_code'     =>  $this->customerCode
        ];

        // $cards = $this->client->getCards(['customer_code'=>$this->customerCode], $this->customerCode);
        $cardData = [
            "number"        =>  "4030000010001234",
            "expiry_month"  =>  "02",
            "expiry_year"   =>  "20",
            "cvd"           =>  "123"
        ];
        $data = [
            'amount'            =>  23.32,
            'payment_method'    => 'card',
            'card'              =>  $cardData
        ];
        
        $paid = $this->client->processPayment($data, $this->customerCode);
    }

    /**
     */
    public function testCreateChargeForNewCustomer()
    {
    }

    /**
     */
    public function testCreditCardExpires()
    {
    }

    private function assertStructure($structure, $response)
    {
        $valid = true;
        foreach ($structure as $key) {
            if (!array_key_exists($key, $response)) {
                $valid = false;
            }
        }

        return $valid;
    }
}
