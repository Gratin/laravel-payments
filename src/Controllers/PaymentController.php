<?php
namespace Gratin\Payment\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Gratin\Payment\Models\PaypalEvent;

class PaymentController extends Controller
{
    const PAYPAL            = "paypal";
    const PAYPAL_FORM_PATH  = "gratinayment::paypal.paypal-form";
    
    public function paypal()
    {
        $configs    = $this->config(self::PAYPAL);

        $mode       = $configs['mode'] ?? 'sandbox';

        try {
            $key    = $configs[$mode];
        } catch (\Exception $e) {
            throw new \Exception('mode_key_not_set :: '. $mode);
        }

        return view()->make(
            static::PAYPAL_FORM_PATH,
            [
                'sandbox_token' => $key,
            ]
        );
    }

    public function event(Request $request)
    {
        $data = $request->getContent();

        try {
            $data = json_decode($data);
        } catch (\Exception $e) {
        }

        // switch ($data->event_type) {
        //     case self::PAYPAL_
        // }

        try {
            $event = PaypalEvent::create([
                'event_type'    =>  $data->event_type,
                'payload'       =>  serialize($data),
                'paypal_id'     =>  $data->id,
            ]);
        } catch (\Exception $e) {
        }

        $resource = $data->resource;

        switch ($resource->state) {
            case "authorized":
                break;
            case "pending":
                break;
        }

        $resourceId = $resource->id;
        $dataId = $data->id;
        $total = $resource->amount->total;
        $currency = $resource->amount->currency;

        //  TO DO : Le reste
    }

    private function config(string $service)
    {
        if (!$service) {
            throw new \Exception('no_service_defined in ' . get_class($this));
        }

        try {
            $configs = config('payment.gateway.'.$service);
        } catch (\Exception $e) {
            throw new \Exception('cant_load_config :: '. $service);
        }

        return $configs;
    }
}
