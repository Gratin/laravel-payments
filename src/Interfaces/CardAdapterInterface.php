<?php
namespace Gratin\Payment\Interfaces;

interface CardAdapterInterface extends BaseAdapterInterface
{
    public function createCard(array $data): array;
    public function getCards(array $payload, string $customerId): array;
    public function getCard(array $payload, string $customerId): array;
}
