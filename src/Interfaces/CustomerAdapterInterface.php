<?php
namespace Gratin\Payment\Interfaces;

interface CustomerAdapterInterface extends BaseAdapterInterface
{
    public function createCustomer(array $data): array;
    public function getCustomer(string $customerId): array;
}
