<?php
namespace Gratin\Payment\Interfaces;

interface BaseAdapterInterface
{
    public function config(): void;
    public function processPayment(array $payload): array;
}
