<?php
namespace Gratin\Payment\Adapters\Bambora;

use Gratin\Payment\Adapters\BamboraAdapter;
use Gratin\Payment\Interfaces\Bambora\BaseCustomerInterface;

class CustomerAdapter implements BaseCustomerInterface
{
    private $id;
    private $adapter;

    public function __construct(string $customerId)
    {
        $this->id = $customerId;
        $this->adapter = new BamboraAdapter();
    }

    /**
     * Prepares the array data to send to the adapter for a payment for a customer
     * @param float $amount The amount to be billed
     * @param ?int $cardId The credit card Id to use, defaults to the first one of the customer
     * @return array
     */
    public function pay(float $amount, ?int $cardId = null): array
    {
        if (!$cardId) {
            $cards  = $this->adapter->getCards([], $this->id);
            $card   = array_shift($cards['card']);
            $cardId = $card['card_id'];
        }

        $payload = [
            'amount'            =>  number_format($amount, 2),
            'payment_method'    =>  'payment_profile',
            'payment_profile'   =>  [
                'customer_code' =>  $this->id,
                'card_id'       =>  $cardId
            ]
        ];

        try {
            return $this->adapter->processPayment($payload);
        } catch (\Exception $e) {
            throw $e;
            throw new \Exception('payment_payload.invalid');
        }
    }
}
