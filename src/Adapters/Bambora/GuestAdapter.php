<?php
namespace Gratin\Payment\Adapters\Bambora;

use Gratin\Payment\Adapters\BamboraAdapter;
use Gratin\Payment\Interfaces\Bambora\BaseCustomerInterface;

class GuestAdapter implements BaseCustomerInterface
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new BamboraAdapter();
    }

    /**
     * Prepares the array data to send to the adapter for a payment for a guest (without customer)
     * @param float $amount The amount to be billed
     * @param string $token The credit card token. DOES NOT SUPPORT CREDIT CARD ARRAY
     * @param array $billing The customer's billing informations
     * @return array
     */
    public function pay(float $amount, string $token, array $billing): array
    {
        try {
            $payload = [
                'amount'            =>  number_format($amount, 2),
                'payment_method'    =>  'token',
                'token'             =>  [
                    'code'          =>  $token,
                    'name'          =>  $billing['name'],
                ],
                'billing'           =>  $billing
            ];

            // echo json_encode($payload);die;
            return $this->adapter->processPayment($payload);
        } catch (\Exception $e) {
            throw $e;
            // throw new \Exception('guest_payload.invalid');
        }
    }
}
