<?php
namespace Gratin\Payment\Adapters;

use Gratin\Payment\Http\BamboraClient;
use Gratin\Payment\Helpers\YamlLoader;
use Gratin\Payment\Handlers\PaymentErrorHandler;
use Gratin\Payment\Interfaces\CustomerAdapterInterface;
use Gratin\Payment\Interfaces\CardAdapterInterface;

class BamboraAdapter implements CustomerAdapterInterface, CardAdapterInterface
{
    /**
    * @var Gratin\Payment\Http\BamboraClient $client An instance of the Bambora Http Client
    */
    private $client;

    /**
    * @var string $token The generic token to use for Bambora calls
    */
    private $token;

    /**
    * @var string $paymenttoken The payment token to use for Bambora calls
    */
    private $paymentToken;

    public function __construct()
    {
        $this->client = new BamboraClient();
        $this->config();
    }

    /**
     * Configuress the class and generates tokens
     * @return void
     */
    public function config(): void
    {
        $gateway = config('payment.gateway.bambora');
        if (empty($gateway)) {
            PaymentErrorHandler::noConfig('bambora');
        }

        $this->token        = $this->generateToken($gateway['merchantId'], $gateway['passcode']);
        $this->paymentToken = $this->generateToken($gateway['merchantId'], $gateway['payment_passcode']);
    }

    /**
     * Creates a card. INSECURE NOT AVAILABLE IN PRODUCTION ENVIRONEMENT.
     * @param array $testData An array of credit card data to send to Bambora to generate a token
     * @return array
     */
    public function createCard(array $data): array
    {
        if (env('APP_ENV') === "production") {
            PaymentErrorHandler::notInProduction('create.card');
        }

        return $this->client->setAuthorizationHeader($this->paymentToken)->createCard($data)->getContent();
    }

    /**
     * Deletes a customer. INSECURE NOT AVAILABLE IN PRODUCTION ENVIRONEMENT.
     * @param array $testData An array of customer's information to delete
     * @return array
     */
    public function deleteCustomer(array $data): array
    {
        if (env('APP_ENV') === "production") {
            PaymentErrorHandler::notInProduction('customer.delete');
        }

        return $this->client->setAuthorizationHeader($this->paymentToken)->deleteCustomer($data)->getContent();
    }

    /**
     * Creates a customer.
     * @param array $payload An array of customer's information to create. Including it's billing informations
     * @return array
     */
    public function createCustomer(array $payload): array
    {
        return $this->client->setAuthorizationHeader($this->paymentToken)->createCustomer($payload)->getContent();
    }

    /**
     * Processes a payment. Validates the payload and then send the payment request
     * @param array $payload An array of payment's informations to create.
     * @param mixed $user The User to rertrieve the customer's ID from.
     * @return array
     */
    public function processPayment(array $payload): array
    {
        $valid = $this->validPaymentPayload($payload);

        return $this->client->setAuthorizationHeader($this->token)->pay($payload)->getContent();
    }

    /**
     * Retrieves a customer from a given user
     * @param string $customerId The customer ID.
     * @return array
     */
    public function getCustomer(string $customerId): array
    {
        return $this->client->setAuthorizationHeader($this->paymentToken)->getCustomer($customerId)->getContent();
    }

    /**
     * Retrieves a customer's credit cards
     * @param mixed $payload An array of the request's payload
     * @param string $customerCode The customer code of whom we should retrieve credit cards
     * @return array
     */
    public function getCards(array $payload, string $customerCode): array
    {
        return $this->client->setAuthorizationHeader($this->paymentToken)->cards($payload, $customerCode)->getContent();
    }

    /**
     * Retrieves a customer's credit card
     * @param mixed $payload An array of the request's payload
     * @param string $customerCode The customer code of whom we should retrieve credit cards
     * @return array
     */
    public function getCard(array $payload, string $customerCode): array
    {
        return $this->client->setAuthorizationHeader($this->paymentToken)->cards($payload, $customerCode)->getContent();
    }

    /**
     * Validates that the basic minimum for the payment request are met
     * @param array $payload The request payload
     * @return bool
     */
    private function validPaymentPayload(array $payload): bool
    {
        if (!array_key_exists('amount', $payload) ||
            !array_key_exists('payment_method', $payload)
            //|| !array_key_exists('token', $payload)
        ) {
            PaymentErrorHandler::invalidPaymentPayload();
        }

        return true;
    }

    private function formatCustomerData($raw, bool $payment = false)
    {
        $prefix = $raw['prefix'] ?? 'billing_';
        $customerBilling = [
            'name'=> $raw['name'] ?? 'John Doe',
            'address_line1'=>$raw['billing']['address'],
            'city'=>$raw['billing']['city'],
            'province'=>$raw['billing']['state'],
            'country'=>'CA',
            'postal_code'=>$raw['billing']['zip'],
            'email_address'=>$raw['email'],
            'phone_number'  =>'514-555-5555'
        ];

        return $customerBilling;
    }

    /**
     * Generates and base64_encode tokens
     * @param string $merchantId The account's merchant Id
     * @param string $passcode The scoped Bambora passcode
     * @return string
     */
    private function generateToken(string $merchantId, string $passcode): string
    {
        if (empty($merchantId) || empty($passcode)) {
            PaymentErrorHandler::noConfig('bambora.gateway');
        }

        return "Passcode ". base64_encode($merchantId.":".$passcode);
    }

    public function guest()
    {
        return new GuestAdapter();
    }
}
