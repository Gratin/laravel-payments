<?php
namespace Gratin\Payment\Adapters;

use Gratin\Payment\Interfaces\BaseAdapterInterface;

class PaypalAdapter implements BaseAdapterInterface
{
    public function config(): void
    {
    }

    public function processPayment(array $payload): array
    {
        return ['', ''];
    }

    public function handleErrors($response)
    {
        // TODO Validate functionality
        $statusCode = $response->status;
        switch ($statusCode) {
            case 400:
                $this->getErrorMessage($response);
                break;
            case 401:
                //
                break;
            case 403:
                //
                break;
            case 429:
                // TOO MANY REQUEST
                break;
            default:
                break;
        }
    }

    private function getErrorMessage($response)
    {
        // TODO it
        return $response;
    }
}
