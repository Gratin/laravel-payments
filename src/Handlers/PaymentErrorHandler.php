<?php
namespace Gratin\Payment\Handlers;

use Exception;

class PaymentErrorHandler extends Exception
{
    public const NO_CONFIG      = "errors.payment.config.unavailable";
    public const BAD_CONFIG     = "errors.payment.config.wrong";
    public const ACTION_FAILED  = "errors.payment.action_failed";
    public const NIP            = "errors.payment.not_in_production";

    public static function noConfig($key)
    {
        throw new self(__(static::NO_CONFIG, ['key'=>$key]));
    }

    public static function actionFailed($key)
    {
        throw new self(__(static::ACTION_FAILED, ['action'=>$key]));
    }

    public static function notInProduction($key)
    {
        throw new self(__(static::NIP, ['action'=>$key]));
    }

    public static function invalidPaymentPayload()
    {
        throw new self(__(static::NIP));
    }

    public static function invalidExpiration()
    {
        throw new self(__(static::EXPIRATION_INVALID));
    }

    public static function invalidTransaction()
    {
        throw new self(__(static::INVALID_TRANSACTION));
    }

    public static function cardDeclined()
    {
        throw new self(__(static::CARD_DECLINED));
    }

    public static function invalidCardNumber()
    {
        throw new self(__(static::CARD_NUMBER_INVALID));
    }

    public static function invalidTransactionAmount()
    {
        throw new self(__(static::INVALID_TRANSACTION_AMOUNT));
    }
}
