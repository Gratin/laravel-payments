<?php
return [
    'gateway'   =>  [
        'bambora'   =>  [
            'merchantId'            =>  env('BABMORA_MERCHANT_ID', ""),
            "passcode"              =>  env('BAMBORA_PASSCODE', ""),
            "payment_passcode"      =>  env('BAMBORA_PAYMENT_PASSCODE', ""),
        ],
        'paypal'    =>  [
            'sandbox'   =>  env('PAYPAL_SANDBOX_KEY', ""),
            'production'=>  env('PAYPAL_PRODUCTION_KEY', ""),
            'mode'      =>  env('PAYPAL_MODE', "sandbox"),
        ]
    ]
];
