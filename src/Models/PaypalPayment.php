<?php
namespace Gratin\Payment\Models;

use Illuminate\Database\Eloquent\Model;

class PaypalPayment extends Model
{
    public $table = "paypal_payments";

    public $timestamps = true;

    public $fillable = [
        'data',
    ];
}
