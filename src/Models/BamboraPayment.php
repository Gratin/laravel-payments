<?php
namespace Gratin\Payment\Models;

use Illuminate\Database\Eloquent\Model;

class BamboraPayment extends Model
{
    public $table = "bambora_payments";

    public $timestamps = true;

    public $fillable = [
        'data',
    ];
}
