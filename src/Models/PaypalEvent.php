<?php
namespace Gratin\Payment\Models;

use Illuminate\Database\Eloquent\Model;

class PaypalEvent extends Model
{
    public $table = "paypal_events";

    public $timestamps = true;

    public $fillable = [
        'event_type',
        'payload',
        'paypal_id'
    ];
}
