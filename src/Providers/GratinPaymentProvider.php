<?php

namespace Gratin\Payment\Providers;

use Illuminate\Support\ServiceProvider;

class GratinPaymentProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/payment.php');

        $this->publishes([
            __DIR__.'/../config/payment.php' => config_path('payment.php'),
        ]);

        $this->loadViewsFrom(__DIR__.'/../resources', 'gratinayment');

        $this->publishes([
            __DIR__.'/../resources' => resource_path('views/vendor/gratinayment'),
        ]);

        $this->loadMigrationsFrom(__DIR__.'/../migrations');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/payment.php',
            'payment'
        );
    }
}
