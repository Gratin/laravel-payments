<?php
namespace Gratin\Payment\Http;

use GuzzleHttp\Client;
use Gratin\Payment\Handlers\PaymentErrorHandler;

class BamboraClient
{
    /**
     * @var GuzzleHttp\Client $client An instance of Guzzle Http Client
     */
    private $client;

    /**
     * @var GuzzleHttp\Psr7\Response $response An instance of Guzzle Http Response
     */
    private $response;

    /**
     * @var string $url The base Bambora api URL
     */
    private $url        = "https://api.na.bambora.com/v1/%s";

    /**
     * @var string $scripts The base Bambora scripts URL
     */
    private $scripts    = "https://api.na.bambora.com/scripts/%s";

    /**
     * @var array $headers The base Bambora api request headers
     */
    private $headers    = [
        'Content-Type'  => 'application/json',
        'Authorization' => null,
    ];

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Sets the requests' authorization header to the given token
     * @param string $header The header value
     * @return BamboraClient
     */
    public function setAuthorizationHeader($header): BamboraClient
    {
        $this->headers['Authorization'] = $header;

        return $this;
    }

    /**
     * Retrieves the response's content
     * @return array json_decoded string of the response's body
     */
    public function getContent(): array
    {
        try {
            $this->handleExceptions($this->response);
        } catch (\Exception $e) {
            throw $e;
        }

        return @json_decode($this->response->getBody()->getContents(), true) ?? [];
    }

    /**
     * Launch a customer creation request
     * @param array $payload The array containing informations to create the customer
     * @return BamboraClient
     */
    public function createCustomer(array $payload): BamboraClient
    {
        try {
            $this->response = $this->client->post($this->url('profiles'), ['json'=>$payload, 'headers'=>$this->headers]);
        } catch (\Exception $e) {
            throw $e;
            return PaymentErrorHandler::actionFailed('client.create');
        }

        return $this;
    }

    /**
     * Launch a customer retrieval request
     * @param string $customerId The customer id to retrieve
     * @return BamboraClient
     */
    public function getCustomer($customerId): BamboraClient
    {
        try {
            $this->response = $this->client->get($this->url('profiles/'.$customerId), ['headers'=>$this->headers]);
        } catch (\Exception $e) {
            throw new $e;
            return PaymentErrorHandler::actionFailed('client.get');
        }

        return $this;
    }

    /**
     * Launch a credit card creation request. INSECURE SHOULD NOT BE CALLED IN PRODUCTION
     * @param array $payload The payload containing information to create the credit card
     * @return BamboraClient
     */
    public function createCard(array $payload): BamboraClient
    {
        if (env('APP_ENV') === "production") {
            return PaymentErrorHandler::notInProduction('create.card');
        }

        try {
            $this->response = $this->client->post($this->script('tokenization/tokens'), ['json'=>$payload, 'headers'=>$this->headers]);
        } catch (\Exception $e) {
            throw $e;
        }

        return $this;
    }

    /**
     * Launch a credit card retrieval request.
     * @param array $payload The payload containing information
     * @param string $customerId The customer id from whom to retrieve credit cards
     * @return BamboraClient
     */
    public function cards(array $payload, string $customerId): BamboraClient
    {
        try {
            $this->response = $this->client->get($this->url('profiles/'.$customerId.'/cards'), ['json'=>$payload, 'headers'=>$this->headers]);
        } catch (\Exception $e) {
            throw $e;
        }

        return $this;
    }

    /**
     * Launch a credit card retrieval request.
     * @param array $payload The payload containing information
     * @param string $customerId The customer id from whom to retrieve credit cards
     * @param string $cardId The credit card identifier
     * @return BamboraClient
     */
    public function card(array $payload, string $customerId, string $cardId): BamboraClient
    {
        try {
            $this->response = $this->client->get($this->url('profiles/'.$customerId.'/cards/'.$cardId), ['json'=>$payload, 'headers'=>$this->headers]);
        } catch (\Exception $e) {
            throw $e;
        }

        return $this;
    }

    /**
     * Launch a customer deletion request. INSECURE SHOULD NOT BE CALLED IN PRODUCTION
     * @param array $payload The payload containing information
     * @return BamboraClient
     */
    public function deleteCustomer(array $payload): BamboraClient
    {
        if (env('APP_ENV') === "production") {
            return PaymentErrorHandler::notInProduction('customer.delete');
        }

        try {
            $this->response = $this->client->delete($this->url('profiles/delete'), ['json'=>$payload, 'headers'=>$this->headers]);
        } catch (\Exception $e) {
            throw $e;
        }

        return $this;
    }

    /**
     * Launch a payment request
     * @param array $payload The payload containing information
     * @return BamboraClient
     */
    public function pay(array $payload): BamboraClient
    {
        try {
            $this->response = $this->client->post($this->url('payments'), ['json'=>$payload, 'headers'=>$this->headers]);
        } catch (\Exception $e) {
            throw $e;
        }

        return $this;
    }

    /**
     * Builds a Bambora request URL
     * @param string $key The key to append to the base url
     * @return string
     */
    private function url(string $key): string
    {
        return sprintf($this->url, $key);
    }

    /**
     * Builds a Bambora script URL
     * @param string $key The key to append to the base script url
     * @return string
     */
    private function script(string $key): string
    {
        return sprintf($this->scripts, $key);
    }

    /**
     * Handles Bambora Exception codes
     * @param GuzzleHttp\Psr7\Response $response The response from the request
     * @return GuzzleHttp\Psr7\Response
     */
    private function handleExceptions($response)
    {
        if (empty($response)) {
            throw new \Exception('error.unknown');
        }

        if ($response->getStatusCode() === 200) {
            return $response;
        }

        try {
            $content = json_decode($response, true);
        } catch (\Exception $e) {
            throw new \Excpetion('Malformed response');
        }

        switch (true) {
            case $content['code'] === 14:
            case $content['code'] === 26:
                // invalid expiration
                PaymentErrorHandler::invalidExpiration();
                break;
            case $content['code'] === 16:
            case $content['code'] === 49:
            case $content['code'] === 50:
                // invalid transaction
                PaymentErrorHandler::invalidTransaction();
                break;
            case $content['code'] > 50 && $content['code'] < 189:
                // Declined
                PaymentErrorHandler::cardDeclined();
                break;
            case $content['code'] === 52:
                // Invalid card number
                PaymentErrorHandler::invalidCardNumber();
                break;
            case $content['code'] === 191:
            case $content['code'] === 194:
                // Over allowed amount
                PaymentErrorHandler::invalidTransactionAmount();
                break;
        }

        return $response;
    }
}
